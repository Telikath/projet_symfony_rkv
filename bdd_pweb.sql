-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3307
-- Généré le : mer. 02 juin 2021 à 08:59
-- Version du serveur :  10.5.4-MariaDB
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bdd_pweb`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `privacy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `name`, `privacy`, `media`) VALUES
(1, 'Article de lancement', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'img/categories/article.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `article_user`
--

DROP TABLE IF EXISTS `article_user`;
CREATE TABLE IF NOT EXISTS `article_user` (
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`user_id`),
  KEY `IDX_3DD151487294869C` (`article_id`),
  KEY `IDX_3DD15148A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `article_user`
--

INSERT INTO `article_user` (`article_id`, `user_id`) VALUES
(1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210520224119', '2021-05-20 22:41:23', 147),
('DoctrineMigrations\\Version20210520224534', '2021-05-20 22:45:39', 125),
('DoctrineMigrations\\Version20210520224812', '2021-05-20 22:48:16', 121),
('DoctrineMigrations\\Version20210520225417', '2021-05-20 22:54:20', 179),
('DoctrineMigrations\\Version20210520230305', '2021-05-20 23:03:10', 286),
('DoctrineMigrations\\Version20210520230803', '2021-05-20 23:08:07', 144),
('DoctrineMigrations\\Version20210520231035', '2021-05-20 23:10:39', 50),
('DoctrineMigrations\\Version20210522170431', '2021-05-22 17:04:37', 2423),
('DoctrineMigrations\\Version20210524152344', '2021-05-24 15:24:01', 214),
('DoctrineMigrations\\Version20210526092412', '2021-05-26 09:25:31', 300),
('DoctrineMigrations\\Version20210526114632', '2021-05-26 11:46:49', 21);

-- --------------------------------------------------------

--
-- Structure de la table `emploidutemps`
--

DROP TABLE IF EXISTS `emploidutemps`;
CREATE TABLE IF NOT EXISTS `emploidutemps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `emploidutemps`
--

INSERT INTO `emploidutemps` (`id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Structure de la table `emploidutemps_user`
--

DROP TABLE IF EXISTS `emploidutemps_user`;
CREATE TABLE IF NOT EXISTS `emploidutemps_user` (
  `emploidutemps_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`emploidutemps_id`,`user_id`),
  KEY `IDX_FF20253F207496AC` (`emploidutemps_id`),
  KEY `IDX_FF20253FA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `emploidutemps_user`
--

INSERT INTO `emploidutemps_user` (`emploidutemps_id`, `user_id`) VALUES
(1, 3),
(1, 6),
(2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
CREATE TABLE IF NOT EXISTS `evenement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_debut` bigint(20) NOT NULL,
  `date_fin` bigint(20) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_day` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`id`, `name`, `description`, `date_debut`, `date_fin`, `url`, `all_day`) VALUES
(1, 'Event', 'Description', 1622028600, 1622041200, '/upload/data.txt', 1);

-- --------------------------------------------------------

--
-- Structure de la table `evenement_promotion`
--

DROP TABLE IF EXISTS `evenement_promotion`;
CREATE TABLE IF NOT EXISTS `evenement_promotion` (
  `evenement_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  PRIMARY KEY (`evenement_id`,`promotion_id`),
  KEY `IDX_6F437100FD02F13` (`evenement_id`),
  KEY `IDX_6F437100139DF194` (`promotion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `evenement_promotion`
--

INSERT INTO `evenement_promotion` (`evenement_id`, `promotion_id`) VALUES
(1, 1),
(1, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
CREATE TABLE IF NOT EXISTS `matiere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`id`, `name`) VALUES
(1, 'Mathematiques'),
(2, 'Musique'),
(3, 'Algorithmique'),
(4, 'Anglais'),
(5, 'Traitement du signal');

-- --------------------------------------------------------

--
-- Structure de la table `matiere_user`
--

DROP TABLE IF EXISTS `matiere_user`;
CREATE TABLE IF NOT EXISTS `matiere_user` (
  `matiere_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`matiere_id`,`user_id`),
  KEY `IDX_FE415017F46CD258` (`matiere_id`),
  KEY `IDX_FE415017A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `matiere_user`
--

INSERT INTO `matiere_user` (`matiere_id`, `user_id`) VALUES
(5, 5);

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idmatiere_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CFBDFA1439C5CF62` (`idmatiere_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `note`
--

INSERT INTO `note` (`id`, `idmatiere_id`, `value`) VALUES
(1, 4, 18),
(2, 2, 2),
(3, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `note_user`
--

DROP TABLE IF EXISTS `note_user`;
CREATE TABLE IF NOT EXISTS `note_user` (
  `note_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`note_id`,`user_id`),
  KEY `IDX_2DE9C71126ED0855` (`note_id`),
  KEY `IDX_2DE9C711A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `note_user`
--

INSERT INTO `note_user` (`note_id`, `user_id`) VALUES
(1, 6),
(2, 3),
(3, 2),
(3, 3),
(3, 4);

-- --------------------------------------------------------

--
-- Structure de la table `portfolio`
--

DROP TABLE IF EXISTS `portfolio`;
CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid_id` int(11) NOT NULL,
  `cv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cv_visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A9ED106258E0A285` (`userid_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `portfolio`
--

INSERT INTO `portfolio` (`id`, `userid_id`, `cv`, `cv_visible`) VALUES
(1, 3, 'cv', 1),
(2, 2, 'qsdqsds', 1);

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_projet`
--

DROP TABLE IF EXISTS `portfolio_projet`;
CREATE TABLE IF NOT EXISTS `portfolio_projet` (
  `projet_id` int(11) NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  PRIMARY KEY (`projet_id`,`portfolio_id`),
  KEY `IDX_56FE719CC18272` (`projet_id`),
  KEY `IDX_56FE719CB96B5643` (`portfolio_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `portfolio_projet`
--

INSERT INTO `portfolio_projet` (`projet_id`, `portfolio_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `projet`
--

INSERT INTO `projet` (`id`, `name`) VALUES
(1, 'RKV'),
(2, 'sdfsdf');

-- --------------------------------------------------------

--
-- Structure de la table `projet_user`
--

DROP TABLE IF EXISTS `projet_user`;
CREATE TABLE IF NOT EXISTS `projet_user` (
  `projet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`projet_id`,`user_id`),
  KEY `IDX_FA413966C18272` (`projet_id`),
  KEY `IDX_FA413966A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `projet_user`
--

INSERT INTO `projet_user` (`projet_id`, `user_id`) VALUES
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

DROP TABLE IF EXISTS `promotion`;
CREATE TABLE IF NOT EXISTS `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annee` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `promotion`
--

INSERT INTO `promotion` (`id`, `annee`) VALUES
(1, 2019),
(2, 2020),
(3, 2021);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_creation` int(11) NOT NULL,
  `date_derniere_conn` int(11) NOT NULL,
  `idpromotion_id` int(11) DEFAULT NULL,
  `idportfolio_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  UNIQUE KEY `UNIQ_8D93D649FFC4294A` (`idportfolio_id`),
  KEY `UNIQ_8D93D64955328E9D` (`idpromotion_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `firstname`, `lastname`, `date_creation`, `date_derniere_conn`, `idpromotion_id`, `idportfolio_id`) VALUES
(2, 'admin@mail.com', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$Z0RzWEhPejc3ZnhwSjJwcA$PhgTul2/fd5EJ5rGoMyNaV4/Sk2MnmjevHmZjlH699c', 'admin', 'admin', 1, 1, NULL, NULL),
(3, 'student@mail.com', '[\"ROLE_STUDENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$M3FOVDZ3bXNIR2VKaXIuTA$wVJWAcipEf2XhfCY+VqsLMNJ4uk1ErVte5JFk8NenmU', 'Student', 'Student', 1, 1, 2, NULL),
(4, 'bde@mail.com', '{\"0\":\"ROLE_ADMIN_BDE\",\"2\":\"ROLE_STUDENT\"}', '$argon2id$v=19$m=65536,t=4,p=1$eU5QLm1Zb1NwL2x3WmNXdg$ixpk3X/e8WZLQJ5YF2fq9qI+w4YQCsqj4M3ry90JoE4', 'Bde', 'Bde', 1, 1, 1, NULL),
(5, 'enseignant@mail.com', '[\"ROLE_ENSEIGNANT\"]', '$argon2id$v=19$m=65536,t=4,p=1$WFZSeHJ6MWVXZENMM2djTQ$MWCg/3sGuX1my19X0V1qWEsvc3biZta+wnCwl4rFBYQ', 'Enseignant', 'Enseignant', 1, 1, NULL, NULL),
(6, 'delegue@mail.com', '[\"ROLE_STUDENT\",\"ROLE_DELEGUE\"]', '$argon2id$v=19$m=65536,t=4,p=1$ZlFaQzRGcmVxZk4wUDYuVA$AhjdTeucgglpxeWbVjit4IvQJ9lYw3ljtGz10yfSP3w', 'Delegue', 'Delegue', 1, 1, 3, NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article_user`
--
ALTER TABLE `article_user`
  ADD CONSTRAINT `FK_3DD151487294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_3DD15148A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `emploidutemps_user`
--
ALTER TABLE `emploidutemps_user`
  ADD CONSTRAINT `FK_FF20253F207496AC` FOREIGN KEY (`emploidutemps_id`) REFERENCES `emploidutemps` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FF20253FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `evenement_promotion`
--
ALTER TABLE `evenement_promotion`
  ADD CONSTRAINT `FK_6F437100139DF194` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6F437100FD02F13` FOREIGN KEY (`evenement_id`) REFERENCES `evenement` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `matiere_user`
--
ALTER TABLE `matiere_user`
  ADD CONSTRAINT `FK_FE415017A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FE415017F46CD258` FOREIGN KEY (`matiere_id`) REFERENCES `matiere` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `FK_CFBDFA1439C5CF62` FOREIGN KEY (`idmatiere_id`) REFERENCES `matiere` (`id`);

--
-- Contraintes pour la table `note_user`
--
ALTER TABLE `note_user`
  ADD CONSTRAINT `FK_2DE9C71126ED0855` FOREIGN KEY (`note_id`) REFERENCES `note` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2DE9C711A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `portfolio`
--
ALTER TABLE `portfolio`
  ADD CONSTRAINT `FK_A9ED106258E0A285` FOREIGN KEY (`userid_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `portfolio_projet`
--
ALTER TABLE `portfolio_projet`
  ADD CONSTRAINT `FK_56FE719CB96B5643` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolio` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_56FE719CC18272` FOREIGN KEY (`projet_id`) REFERENCES `projet` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `projet_user`
--
ALTER TABLE `projet_user`
  ADD CONSTRAINT `FK_FA413966A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FA413966C18272` FOREIGN KEY (`projet_id`) REFERENCES `projet` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D64955328E9D` FOREIGN KEY (`idpromotion_id`) REFERENCES `promotion` (`id`),
  ADD CONSTRAINT `FK_8D93D649FFC4294A` FOREIGN KEY (`idportfolio_id`) REFERENCES `portfolio` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
