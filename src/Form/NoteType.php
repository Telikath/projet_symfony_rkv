<?php

namespace App\Form;

use App\Entity\Note;
use App\Entity\Matiere;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class NoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value')
            ->add('idmatiere', EntityType::class, [
                
                
                'class' => Matiere::class,
                'choice_label' => 'name',
                'label' => 'Matiere'
                
            ])
            ->add('userid', EntityType::class, [
                
                
                'class' => User::class,
                'choice_label' => 'email',
                'multiple' => true,
                'label' => 'User'
            ])
           // ->add('userid')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Note::class,
        ]);
    }
}
