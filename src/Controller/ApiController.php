<?php

namespace App\Controller;

use App\Entity\Evenement;

use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/event/new", name="api_event_new", methods={"POST"})
     */
    public function newEvent(Request $request, FileUploader $fileUploader): Response
    {
        $donnees = json_decode($request->getContent());

        if(
            isset($donnees->title) && !empty($donnees->title) &&
            isset($donnees->start) && !empty($donnees->start) &&
            isset($donnees->description) && !empty($donnees->description) &&
            isset($donnees->end) && !empty($donnees->end) &&
            isset($donnees->allDay)
        ){
            $code = 201;

            $calendar = new Evenement;

            //On hydrate l'objet
            $calendar->setName($donnees->title);
            $calendar->setDescription($donnees->description);
            $start = new \DateTime($donnees->start);
            $calendar->setDateDebut($start->getTimestamp());
            $end = new \DateTime($donnees->end);
            $calendar->setDateFin($end->getTimestamp());
            $calendar->setAllDay($donnees->allDay);


                $EntityManager = $this->getDoctrine()->getManager();
                $EntityManager->persist($calendar);
                $EntityManager->flush();



            return new Response('Ok ', $code);

        }else{
            return new Response('Données incomplètes', 206);
        }
    }
    /**
     * @Route("/api/event/{id}/edit", name="api_event_edit", methods={"PUT"})
     */
    public function majEvent(?Evenement $calendar, Request $request, FileUploader $fileUploader): Response
    {
        $donnees = json_decode($request->getContent());


        if(
            isset($donnees->title) && !empty($donnees->title) &&
            isset($donnees->start) && !empty($donnees->start) &&
            isset($donnees->description) && !empty($donnees->description) &&
            isset($donnees->end) && !empty($donnees->end) &&
            isset($donnees->allDay)
        ){
            $code = 200;

           if(!$calendar){
               $calendar = new Evenement;
               $code = 201;
           }


           //On hydrate l'objet
            $calendar->setName($donnees->title);
            $calendar->setDescription($donnees->description);
            $start = new \DateTime($donnees->start);
            $calendar->setDateDebut($start->getTimestamp());
            $end = new \DateTime($donnees->end);
            $calendar->setDateFin($end->getTimestamp());

                $EntityManager = $this->getDoctrine()->getManager();
                $EntityManager->persist($calendar);
                $EntityManager->flush();
                //Gestion de l'upload d'un fichier


            return new Response('Ok ', $code);

        }else{
            return new Response('Données incomplètes', 206);
        }
    }
}
