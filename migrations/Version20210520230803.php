<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210520230803 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE promotion (id INT AUTO_INCREMENT NOT NULL, annee INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD idpromotion_id INT NOT NULL, ADD idportfolio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64955328E9D FOREIGN KEY (idpromotion_id) REFERENCES promotion (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649FFC4294A FOREIGN KEY (idportfolio_id) REFERENCES portfolio (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64955328E9D ON user (idpromotion_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649FFC4294A ON user (idportfolio_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64955328E9D');
        $this->addSql('DROP TABLE promotion');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FFC4294A');
        $this->addSql('DROP INDEX UNIQ_8D93D64955328E9D ON user');
        $this->addSql('DROP INDEX UNIQ_8D93D649FFC4294A ON user');
        $this->addSql('ALTER TABLE user DROP idpromotion_id, DROP idportfolio_id');
    }
}
