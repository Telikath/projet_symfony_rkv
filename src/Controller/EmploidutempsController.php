<?php

namespace App\Controller;

use App\Entity\Emploidutemps;
use App\Form\EmploidutempsType;
use App\Repository\EmploidutempsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_ENSEIGNANT')" , statusCode=403, message="Accès refusé")
 * @Route("/emploidutemps")
 */
class EmploidutempsController extends AbstractController
{
    /**
     * @Route("/", name="emploidutemps_index", methods={"GET"})
     */
    public function index(EmploidutempsRepository $emploidutempsRepository): Response
    {
        return $this->render('emploidutemps/index.html.twig', [
            'emploidutemps' => $emploidutempsRepository->findAll(),
            'type' => 'index',
            'level' => 'Emploi du temps', 
        
        ]);
    }

    /**
     * @Route("/new", name="emploidutemps_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $emploidutemp = new Emploidutemps();
        $form = $this->createForm(EmploidutempsType::class, $emploidutemp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($emploidutemp);
            $entityManager->flush();

            return $this->redirectToRoute('emploidutemps_index');
        }

        return $this->render('emploidutemps/new.html.twig', [
            'emploidutemp' => $emploidutemp,
            'form' => $form->createView(),
            'type' => 'admin',
            'level_last' => 'Emploi du temps', 
            'level' => 'Ajout',
            'route_last' => 'emploidutemps_index',
        ]);
    }

    /**
     * @Route("/{id}", name="emploidutemps_show", methods={"GET"})
     */
    public function show(Emploidutemps $emploidutemp): Response
    {
        return $this->render('emploidutemps/show.html.twig', [
            'emploidutemp' => $emploidutemp,
            'type' => 'admin',
            'level_last' => 'Emploi du temps', 
            'level' => 'Voir',
            'route_last' => 'emploidutemps_index',
        ]);
    }

    /**
     * @Route("/{id}/edit", name="emploidutemps_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Emploidutemps $emploidutemp): Response
    {
        $form = $this->createForm(EmploidutempsType::class, $emploidutemp);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('emploidutemps_index');
        }

        return $this->render('emploidutemps/edit.html.twig', [
            'emploidutemp' => $emploidutemp,
            'form' => $form->createView(),
            'type' => 'admin',
            'level_last' => 'Emploi du temps', 
            'level' => 'Modifier',
            'route_last' => 'emploidutemps_index',
        ]);
    }

    /**
     * @Route("/{id}", name="emploidutemps_delete", methods={"POST"})
     */
    public function delete(Request $request, Emploidutemps $emploidutemp): Response
    {
        if ($this->isCsrfTokenValid('delete'.$emploidutemp->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($emploidutemp);
            $entityManager->flush();
        }

        return $this->redirectToRoute('emploidutemps_index');
    }
}
