# Projet_Symfony_RKV

### Année 
2019 / 2022

### Description
L’objectif de ce cours est d'enseigner les bases de la programmation orientée Web à l’aide de technologie tel que PHP, HTML, CSS et de framework tel que Symfony.

Le projet est un site développé par groupe de 3 élèves comportant plusieurs fonctionnalités pouvant être utiles aux personnes faisant partie du CNAM.

### Type :
Application web CNAM

### Liste des développeurs

- Remi CHIANEA
- Killian WYBAUW
- Valentin CIZEAU

### Prérequis
- Symfony
- Composer
- Node.js

### Installation

ouvrir un terminal et entrer:
```bash
~$ git clone https://gitlab.com/Telikath/projet_symfony_rkv.git
~$ cd Projet_Symfony_RKV
~$ composer up
```

### BDD
implementer le fichier bdd_pweb.sql dans une base nommée bdd_pweb

### Activer le serveur Symfony
```bash
~$ Symfony server:start
```

### Log et mots de passe

- Log : admin@mail.com / Mot de passe : admin@1234
- Log : delegue@mail.com / Mot de passe : delegue@1234
- Log : bde@mail.com / Mot de passe : bde@1234
- Log : enseignant@mail.com / Mot de passe : enseignant@1234
- Log : student@mail.com / Mot de passe : student@1234
