<?php

namespace App\Entity;

use App\Repository\PromotionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PromotionRepository::class)
 */
class Promotion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee;

    /**
     * @ORM\ManyToMany(targetEntity=Evenement::class, mappedBy="evenement_promotion")
     */
    private $promotion_evenement;

    public function __construct()
    {
        $this->promotion_evenement = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * @return Collection|Evenement[]
     */
    public function getPromotionEvenement(): Collection
    {
        return $this->promotion_evenement;
    }

    public function addPromotionEvenement(Evenement $promotionEvenement): self
    {
        if (!$this->promotion_evenement->contains($promotionEvenement)) {
            $this->promotion_evenement[] = $promotionEvenement;
            $promotionEvenement->addEvenementPromotion($this);
        }

        return $this;
    }

    public function removePromotionEvenement(Evenement $promotionEvenement): self
    {
        if ($this->promotion_evenement->removeElement($promotionEvenement)) {
            $promotionEvenement->removeEvenementPromotion($this);
        }

        return $this;
    }
}
