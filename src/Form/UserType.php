<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Promotion;
use App\Entity\Portfolio;
use PhpParser\Parser\Multiple;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Admin' => 'ROLE_ADMIN',
                    'Student' => 'ROLE_STUDENT',
                    'BDE' => 'ROLE_ADMIN_BDE',
                    'Enseignant' => 'ROLE_ENSEIGNANT',
                    'Délégué' => 'ROLE_DELEGUE',
                ], 'multiple'=>true,
            ] )
            ->add('password', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('firstname')
            ->add('lastname')
            ->add('idpromotion', EntityType::class, [
                
                
                'class' => Promotion::class,
                'choice_label' => 'annee',
                'label' => 'Promotion'
                
            ])
       
            // ->add('dateCreation')
            // ->add('dateDerniereConn')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
