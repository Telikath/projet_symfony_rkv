<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210520224534 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE emploidutemps (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emploidutemps_user (emploidutemps_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_FF20253F207496AC (emploidutemps_id), INDEX IDX_FF20253FA76ED395 (user_id), PRIMARY KEY(emploidutemps_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE emploidutemps_user ADD CONSTRAINT FK_FF20253F207496AC FOREIGN KEY (emploidutemps_id) REFERENCES emploidutemps (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE emploidutemps_user ADD CONSTRAINT FK_FF20253FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE emploidutemps_user DROP FOREIGN KEY FK_FF20253F207496AC');
        $this->addSql('DROP TABLE emploidutemps');
        $this->addSql('DROP TABLE emploidutemps_user');
    }
}
