<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 *  @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_ENSEIGNANT') or is_granted('ROLE_ADMIN_BDE') or is_granted('ROLE_DELEGUE')" , statusCode=403, message="Accès refusé")
 * @Route("/article")
 */
class ArticleController extends AbstractController
{
    /**
     *  
     * @Route("/", name="article_index", methods={"GET"})
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        return $this->render('article/index.html.twig', [
            'articles' => $articleRepository->findAll(),
            'type' => 'index',
            'level' => 'Article', 
        ]);
    }

    /**
     * @Route("/new", name="article_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserInterface $user): Response
    {
        $article = new Article();
       
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $article->addUserid($user);
            $entityManager->persist($article);
            $entityManager->flush();




            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'type' => 'admin',
            'level_last' => 'Article', 
            'level' => 'Ajout',
            'route_last' => 'article_index',
        ]);
    }

    /**
     * 
     * @Route("/{id}", name="article_show", methods={"GET"})
     */
    public function show(Article $article): Response
    {

        $users[] = $this->getDoctrine()->getRepository(Article::class)->findUserByArticle($article->getId());
        

        return $this->render('article/show.html.twig', [
            'article' => $article,
            'users' => $users[0],
            'type' => 'admin',
            'level_last' => 'Article', 
            'level' => 'Voir',
            'route_last' => 'article_index',
        ]);
    }

    /**
     * 
     * @Route("/{id}/edit", name="article_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Article $article): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'type' => 'admin',
            'level_last' => 'Article', 
            'level' => 'Modifier',
            'route_last' => 'article_index',
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN', statusCode=403, message='Accès refusé') or is_granted('ROLE_ENSEIGNANT', statusCode=403, message='Accès refusé') or is_granted('ROLE_ADMIN_BDE', statusCode=403, message='Accès refusé') or is_granted('ROLE_DELEGUE', statusCode=403, message='Accès refusé')")
     * @Route("/{id}", name="article_delete", methods={"POST"})
     */
    public function delete(Request $request, Article $article): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('article_index');
    }
}
