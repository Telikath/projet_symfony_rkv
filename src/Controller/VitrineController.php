<?php

namespace App\Controller;

use App\Entity\Article;

use App\Entity\Evenement;
use App\Entity\Matiere;

use App\Repository\ArticleRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class VitrineController extends AbstractController
{
    /**
     * @Route("/", name="vitrine")
     */
    public function index(): Response
    {
        return $this->render('vitrine/index.html.twig', [
            'controller_name' => 'VitrineController',
        ]);
    }

    /**
     * @Route("/formations", name="formations")
     */
    public function formations(): Response
    {
        return $this->render('vitrine/formations.html.twig', [
            'controller_name' => 'VitrineController',
            'type' => 'global',
            'level' => 'Formation',
        ]);
    }

    /**
     * @Route("/presentation", name="presentation")
     */
    public function presentation(): Response
    {
        return $this->render('vitrine/presentation.html.twig', [
            'controller_name' => 'VitrineController',
            'type' => 'global',
            'level' => 'Presentation',
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(): Response
    {
        return $this->render('vitrine/contact.html.twig', [
            'controller_name' => 'VitrineController',
            'type' => 'global',
            'level' => 'Contact',
        ]);
    }

    /**
     * @isGranted("ROLE_USER")
     * @Route("/profil", name="profil")
     */
    public function profil(UserInterface $user): Response
    {
        //Gestion de la perte de connexion
        if(!($this->isGranted('ROLE_USER'))){
            return $this->redirectToRoute("vitrine");
        }

        $rdv = array();

        //On regarde si l'utilisateur courant possède un promotion.
        //S'il n'en a pas c'est un admin.

        if($user->getIdpromotion()){
            $events[] = $this->getDoctrine()->getRepository(Evenement::class)->findByPromo($user->getIdpromotion());
            foreach ($events[0] as $e){
                $rdv[] = [
                    'id' => $e['id'],
                    'start' =>  date('Y-m-d H:i:s', $e["date_debut"]),
                    'end' =>  date('Y-m-d H:i:s', $e["date_fin"]),
                    'title' => $e["name"],
                    'description' => $e["description"]
                ];
            }
        }else{
            $events[] = $this->getDoctrine()->getRepository(Evenement::class)->findAll();
            foreach ($events[0] as $e){
                $rdv[] = [
                    'id' => $e->getId(),
                    'start' =>  date('Y-m-d H:i:s', $e->getDateDebut()),
                    'end' =>  date('Y-m-d H:i:s', $e->getDateFin()),
                    'title' => $e->getName(),
                    'description' => $e->getDescription(),
                    'allday'=> $e->getAllDay()
                ];
            }
        }



        $data = json_encode($rdv);

        $matieres[] = $this->getDoctrine()->getRepository(Matiere::class)->findByUser($user->getId());
        return $this->render('vitrine/profil.html.twig', [
            'matieres' => $matieres[0],
            'calendar' => true,
            'data_calendar' => $data,
            'controller_name' => 'VitrineController',
            'type' => 'global',
            'level' => 'Profil',
        ]);
    }

    /**
     * @Route("/news", name="news")
     */
    public function news(): Response
    {
        $myArticles = $this->getDoctrine()->getRepository(Article::class)->findAll();
        return $this->render('vitrine/news.html.twig', [
            'controller_name' => 'VitrineController',
            'type' => 'global',
            'level' => 'News',
            'articles' => $myArticles
        ]);
    }
}