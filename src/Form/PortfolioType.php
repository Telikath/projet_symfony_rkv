<?php

namespace App\Form;

use App\Entity\Portfolio;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Repository\PortfolioRepository;

class PortfolioType extends AbstractType
{
    private $PortfolioRepo;
    public function __construct(PortfolioRepository $PortfolioRepo)
    {
        $this->PortfolioRepo = $PortfolioRepo;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      

        $builder
        
            ->add('cv')
            ->add('userId', EntityType::class, [
                
                
                'class' => User::class,
                'choice_label' => 'email',
                'label' => 'User'
                
            ])
            //->add('userid')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Portfolio::class,
        ]);
    }
}
