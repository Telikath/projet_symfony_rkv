<?php

namespace App\Entity;

use App\Repository\ProjetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjetRepository::class)
 */
class Projet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Portfolio::class)
     * @ORM\JoinTable(name="portfolio_projet")
     */
    private $portfolioid;

    /**
     * @ORM\ManyToMany(targetEntity=User::class)
     * @ORM\JoinTable(name="projet_user")
     */
    private $userid;

    public function __construct()
    {
        $this->portfolioid = new ArrayCollection();
        $this->userid = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Portfolio[]
     */
    public function getPortfolioid(): Collection
    {
        return $this->portfolioid;
    }

    public function addPortfolioid(Portfolio $portfolioid): self
    {
        if (!$this->portfolioid->contains($portfolioid)) {
            $this->portfolioid[] = $portfolioid;
        }

        return $this;
    }

    public function removePortfolioid(Portfolio $portfolioid): self
    {
        $this->portfolioid->removeElement($portfolioid);

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserid(): Collection
    {
        return $this->userid;
    }

    public function addUserid(User $userid): self
    {
        if (!$this->userid->contains($userid)) {
            $this->userid[] = $userid;
        }

        return $this;
    }

    public function removeUserid(User $userid): self
    {
        $this->userid->removeElement($userid);

        return $this;
    }
}
