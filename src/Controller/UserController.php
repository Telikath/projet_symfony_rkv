<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Promotion;
use App\Entity\Matiere;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * 
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN", statusCode=403, message="Accès refusé")
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
            'type' => 'index',
            'level' => 'Utilisateur',
            
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN", statusCode=403, message="Accès refusé")
     * @Route("/page", name="user_page", methods={"GET"})
     */
    public function page(UserRepository $userRepository): Response
    {
        return $this->render('profile.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN", statusCode=403, message="Accès refusé")
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );

            
            $user->setDateCreation();

            $user->setDateDerniereConn();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // encode the plain password

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'type' => 'admin',
            'level_last' => 'Utilisateur', 
            'level' => 'Ajout',
            'route_last' => 'user_index',
            
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN", statusCode=403, message="Accès refusé")
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        

        $matieres[] = $this->getDoctrine()->getRepository(Matiere::class)->findByUser($user->getId());
        return $this->render('user/show.html.twig', [
            'user' => $user,
            'matieres' => $matieres[0],
            'type' => 'admin',
            'level_last' => 'Utilisateur', 
            'level' => 'Voir',
            'route_last' => 'user_index',

        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder, UserInterface $usernow): Response

    {
        if($usernow->getId() == $user->getId() || in_array('ROLE_ADMIN',$usernow->getRoles()) == true )
        {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'type' => 'admin',
            'level_last' => 'Utilisateur', 
            'level' => 'Modifier',
            'route_last' => 'user_index',
        ]);
        }
        return $this->redirectToRoute('user_index');
    }

    /**
     * @IsGranted("ROLE_ADMIN", statusCode=403, message="Accès refusé")
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

}
