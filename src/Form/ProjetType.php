<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Portfolio;
use App\Entity\Projet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProjetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('portfolioid', EntityType::class, [
                
                
                'class' => Portfolio::class,
                'choice_label' => 'cv',
                'multiple' => true,
                'label' => 'Portfolio'
            ])
            ->add('userid', EntityType::class, [
                
                
                'class' => User::class,
                'choice_label' => 'email',
                'multiple' => true,
                'label' => 'User',
            ])
            //->add('userid')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Projet::class,
        ]);
    }
}
